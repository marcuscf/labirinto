/**************************************************
  *                  O LABIRINTO                   *
   *                                                *
    *    CRIADORES                                   *
     *  Leonardo Alves Machado                        *
      *  Marcus Aurelius Cordenunsi Farias             *
       *                                                *
        *  Academicos do curso de Ci�ncia da Computacao  *
         * UFSM, 5o. semestre.                            *
          *                                                *
           *  Contato:                                      *
            *     leo@inf.ufsm.br                            *
             *     marcusy@inf.ufsm.br                        *
              *                                                *
               *                                                * 
                *                          29/09/2002 Versao 1.1 *
                 **************************************************/
#include "mapa.h"
#include <ctype.h>
#include <stdio.h>
#include <string.h>

unsigned char mapa[TM][TM];
Posicao posicaoInicial, posTeletransporte[9][2];
Direcao direcaoInicial;
struct itimerval tempo = { {0L, 0L}, {0L, 0L} };

static bool testa_mapa(void);
static int come_espacos(FILE *arq);

#define MAPA_INVALIDO "ERRO: Mapa com informa��es inv�lidas\n"

// Passa um _nome_ de arquivo com a matriz e armazena no mapa que � global
bool le_mapa(char *nome)
{
	FILE *arq;
	int c, i, j;
        arq = fopen(nome, "r");
        if(arq == NULL)
		return false;
	for (i = 0; i < TM; i++) {
		for (j = 0; j < TM; j++) {
			c = come_espacos(arq);
			if(c == EOF)
				goto mapa_invalido;
                        mapa[i][j] = c;
		}
	}
	c = come_espacos(arq);
	if (c == EOF)
		goto mapa_invalido;
	switch(c){
		case '>': direcaoInicial = DIREITA; break;
		case 'v': direcaoInicial = BAIXO; break;
		case '<': direcaoInicial = ESQUERDA; break;
		case '^': direcaoInicial = CIMA; break;
		default: goto mapa_invalido;
	}
	fscanf(arq, "%ld", &tempo.it_value.tv_sec);
        fclose(arq);
	return testa_mapa();
	
mapa_invalido:
	printf ("%s", MAPA_INVALIDO);
	fclose(arq);
	return false;
}

static
int come_espacos(FILE *arq)
{
	int c;
	do {
		c = fgetc(arq);
	} while ((c != EOF) && isspace(c));
	return c;
}

/*
Ve se o mapa (global) � valido
*/
static
bool testa_mapa (void)
{
	int i, j, inicios = 0, fins = 0, teletransportes[9];
	memset(teletransportes, 0, 9 * sizeof(int));
	for (i = 0; i < TM; i++) {
		for (j = 0; j < TM; j++) {
			if (mapa[i][j] == MAPA_PAREDE || mapa[i][j] == MAPA_CAMINHO || mapa[i][j] == MAPA_BURACO)
				continue;
			if (mapa[i][j] == MAPA_INICIO) {
				inicios++;
				posicaoInicial.x = i;
				posicaoInicial.z = j;
				continue;
			}
			if(isdigit(mapa[i][j]) && mapa[i][j] != '0' ){
				const int n = mapa[i][j] - '0' - 1;
				if (teletransportes[n] != 0) {
                                        posTeletransporte[n][0].x = i;
                                        posTeletransporte[n][0].z = j;
                                }else {
                                        posTeletransporte[n][1].x = i;
                                        posTeletransporte[n][1].z = j;
                                }
                                teletransportes[n]++;
                                continue;
			}
			if (mapa[i][j] == MAPA_FIM) {
				fins++;
				continue;
			} else {
				printf ("%s", MAPA_INVALIDO);
				return false;
			}
		}
	}
	if (fins == 0) {
		fprintf (stderr, "AVISO: Mapa sem sa�da.\n");
	}else if (fins > 1) {
		fprintf (stderr, "AVISO: Mapa com %d sa�das.\n", fins);
	}
	if (inicios != 1) {
		fprintf (stderr, "ERRO: O mapa deve ter exatamente um in�cio.\n");
		return false;
	}
	for(i = 0; i < 9; i++){
		if (!(teletransportes[i] == 0 || teletransportes[i] == 2)) {
			fprintf (stderr, "ERRO: O mapa com n�mero inv�lido de teletransporte.\n");
			return false;
		}
	}
	if (tempo.it_value.tv_sec == 0L) {
		printf ("MODO TREINAMENTO LIGADO\n");
	}
	return true;
}

/* funcao pra caminhar no mapa, 
 * retorna a posicao inicial em caso de parede ou
 * a posicao de destino caso caminho valido
 */
Posicao caminha_mapa (Direcao direcao, Posicao p_ant)
{
	Posicao p_nova = p_ant;
	switch (direcao) {
		case CIMA: p_nova.x--; break;
		case BAIXO: p_nova.x++; break;
		case DIREITA: p_nova.z++; break;
		case ESQUERDA: p_nova.z--; break;
		default: return p_ant;
	}
	if (mapa[p_nova.x][p_nova.z] == MAPA_PAREDE
			|| p_nova.x >= TM || p_nova.z >= TM
			|| p_nova.x < 0 || p_nova.z < 0) {
		return p_ant;
	}
	return p_nova;
}

Direcao vira_esquerda(Direcao d)
{
	if(--d == -1) d = (Direcao)3;
	return d;
}

Direcao vira_direita(Direcao d)
{
	return (Direcao)(((int)d + 1) % 4);
}

// vim: shiftwidth=8 noexpandtab
