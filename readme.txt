Labirinto VERS�O 1.1 (29/09/2002)
por
    Leonardo Alves Machado & Marcus Aurelius Cordenunsi Farias 

Diferen�as entre a vers�o 1 e a 1.1:
- A vers�o 1.1 mostra o tempo restante enquanto voc� joga;
- Pausa: pressione P (ou p) pra pausar e despausar
  Minimizar a janela tamb�m pausa o jogo;
- Modifica��es no c�digo pra compilar tanto como C quanto C++ (casts nas
  fun��es vira_esquerda e vira_direita e inclus�o condicional de stdbool.h);
- A vers�o 1.1 cont�m tamb�m a corre��o do timer inclu�da na vers�o 1.0.1

Conte�do:
- 1 arquivo execut�vel pra Linux ( labir );
- 9 Fases (arquivos .map);
- C�digo fonte (arquivos .c e .h);
- Makefile, pra compilar (no Linux);
- Documenta��o: readme.txt e creditos.txt

Instala��o:
- descompacte o arquivo ( tar -xzf labirinto.tar.gz ou Winzip )
- entre no diret�rio rec�m criado ( cd labirinto )
- digite ./labir *.map

Jogo:
1- comandos do teclado:
   - setas FRENTE ou TR�S: caminhar na dire��o correspondente
   - setas DIREITA ou ESQUERDA: virar 90� para a dire��o desejada
   - ALT + setas DIREITA ou ESQUERDA: deslocamento lateral
   - P ou p: pausa
   - ESC: sair do jogo

2- o mapa
   - Ch�o preto: buracos
   - Ch�o azul mais escuro: teletransportes
   - Ch�o vermelho: sa�da
   - Ch�o esverdeado: inicio

3- Hist�ria
   Voc� foi capturado por extraterrestres e aprisionado em uma dimens�o paralela extremamente gelada. Agora voc� precisa alcan�ar a sa�da por entre caminhos confusos e perturbadores antes que voc� congele.

4- edite seu pr�prio mapa:
   O mapa � composto por exatamente 20 linhas e 20 colunas. Abaixo a lista de caracteres que podem ser usados:
# PAREDE
. CAMINHO
n�meros 1 a 9 TELETRANSPORTES (funcionam em pares)
O BURACOS (Letra mai�scula, n�o o n�mero)
i INICIO
f FIM

   Abaixo do mapa ficam outras informa��es importantes:
> , < , ^ ou v DIRE��O QUE VOC� COME�A OLHANDO
n�mero TEMPO EM SEGUNDOS DO MAPA (opcional)

   Salve seu arquivo com extens�o .map

Eis um exemplo de mapa:

####################
####################
####################
####################
####################
####################
####################
##i...1.O..1...f####
####################
####################
####################
####################
####################
####################
####################
####################
####################
####################
####################
####################
####################
>
30

CONTATO, D�VIDAS, SUGEST�ES E OUTRAS BABOSEIRAS:
leo@inf.ufsm.br
marcusy@inf.ufsm.br
