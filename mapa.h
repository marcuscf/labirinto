/**************************************************
  *                  O LABIRINTO                   *
   *                                                *
    *    CRIADORES                                   *
     *  Leonardo Alves Machado                        *
      *  Marcus Aurelius Cordenunsi Farias             *
       *                                                *
        *  Academicos do curso de Ci�ncia da Computacao  *
         * UFSM, 5o. semestre.                            *
          *                                                *
           *  Contato:                                      *
            *     leo@inf.ufsm.br                            *
             *     marcusy@inf.ufsm.br                        *
              *                                                *
               *                                                * 
                *                          29/09/2002 Versao 1.1 *
                 **************************************************/
#ifndef MAPA_H
#define MAPA_H
#ifndef __cplusplus
  #include <stdbool.h>
#endif
#include <sys/time.h>

/* Informa��es sobre o mapa */

#define TM 20 // Tamanho do mapa
extern unsigned char mapa[TM][TM];

#define MAPA_PAREDE '#'
#define MAPA_CAMINHO '.'
#define MAPA_INICIO 'i'
#define MAPA_FIM 'f'
#define MAPA_BURACO 'O'

/* Os valores da enum Direcao representam as dire��es de quem v� o mapa no
   arquivo, n�o de dentro do jogo nem do OpenGL.
   
   A ordem dos itens na enum Direcao � importante, porque �ngulos s�o
   calculados a partir deles */
typedef
enum Direcao { DIREITA, BAIXO, ESQUERDA, CIMA } Direcao;

typedef
struct Posicao{
  int x, z; // Usado em mapa[x][z]. O resto � feito com translate/rotate
}Posicao;

extern Posicao posicaoInicial, posTeletransporte[9][2];
extern Direcao direcaoInicial;
extern struct itimerval tempo;

/* Fun��es */

/* Abre o arquivo com o nome indicado e seta a dire��o inicial, a posi��o
   inicial e o tempo (se houver). */
bool le_mapa(char *nome);

/* Rota��es de 90� */
Direcao vira_esquerda(Direcao);
Direcao vira_direita(Direcao);

/* Dar um passo. Indicar a Direcao desejada e a Posicao de origem. Retorna a
   Posicao ap�s o passo. */
Posicao caminha_mapa(Direcao, Posicao);

#endif
