/**************************************************
  *                  O LABIRINTO                   *
   *                                                *
    *    CRIADORES                                   *
     *  Leonardo Alves Machado                        *
      *  Marcus Aurelius Cordenunsi Farias             *
       *                                                *
        *  Academicos do curso de Ci�ncia da Computacao  *
         * UFSM, 5o. semestre.                            *
          *                                                *
           *  Contato:                                      *
            *     leo@inf.ufsm.br                            *
             *     marcusy@inf.ufsm.br                        *
              *                                                *
               *                                                * 
                *                          29/09/2002 Versao 1.1 *
                 **************************************************/
#include "mapa.h"
#include <ctype.h>
#include <GL/glut.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

static void display(void);
static void keyboard(unsigned char key, int x, int y);
static void specialkey(int key, int x, int y);
static void reshape(int w, int h);
static void idle(void);
static void visibility(int state);
static void criaLabirinto(void);
static void alarme(int);

static Direcao direcao, oldDirecao;
static Posicao posicao, oldPosicao;
static int totalFases;
static int faseAtual;
static char **fases;

int main(int argc, char **argv)
{
  glutInit(&argc, argv);
  totalFases = argc - 1;
  faseAtual = 1;
  fases = argv;
  if(argc < 2){
    fprintf(stderr, "Uso: %s <arquivo-de-mapa>\n", argv[0]);
    return 1;
  }
  if(!le_mapa(argv[1])){
    fprintf(stderr, "Erro ao carregar fase %s\n", argv[faseAtual]);
    return 1;
  }
  posicao = posicaoInicial;
  direcao = direcaoInicial;
  glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE);
  glutCreateWindow("Labirinto");
  glClearColor(.35f, .35f, .35f, 0.f);
  glEnable(GL_DEPTH_TEST);
  /* N�o desenha os lados internos dos cubos. Tecnicamente melhoraria a
     velocidade. Como ele sabe qual � o lado de dentro? Tem que desenhar os
     v�rtices no sentido anti-hor�rio pra quem olha de fora. */
  glEnable(GL_CULL_FACE);
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutKeyboardFunc(keyboard);
  glutSpecialFunc(specialkey);
  glutIdleFunc(idle);
  glutVisibilityFunc(visibility);
  criaLabirinto();
  signal(SIGALRM, alarme);
  glutMainLoop();
  return 1; // N�o pode chegar aqui...
}

static bool
  rotSuaveEsq = false,
  rotSuaveDir = false,
  passoSuave[4] = { false, false, false, false },
  estaCaindo = false,
  sendoTeletransportado = false,
  seMexendo = false,
  alarmeSetado = false,
  pausado = false;
  
static
void pausa(bool p)
{
  static struct itimerval tempoRestante = { {0L, 0L}, {0L, 0L} };
  /* O if � verdadeiro se:

     1. O novo estado � diferente do antigo (pausa->jogo ou jogo->pausa)

     2. A fase jogada tem tempo

     3. J� come�ou a caminhar (n�o permite pausa antes de come�ar a caminhar na
     fase) */
  if(p != pausado && tempo.it_value.tv_sec != 0 && alarmeSetado){
    pausado = p;
    if(p){
      // Desativa o tempo e salva o que restava
      setitimer(ITIMER_REAL, NULL, &tempoRestante);
      /* A fun��o idle s� serve pra atualizar o tempo. Por isso eu a desativo
         na pausa */
      glutIdleFunc(NULL);
    }else{
      setitimer(ITIMER_REAL, &tempoRestante, NULL);
      glutIdleFunc(idle);
    }
  }
}

static
bool posicoesIguais(Posicao p1, Posicao p2)
{
  return p1.x == p2.x && p1.z == p2.z;
}

static
Posicao getParTele(Posicao pos) // Descobre pra onde o teletransporte vai.
{
  int n = mapa[pos.x][pos.z] - '0' - 1; // N�o checa se � '0'. Cuide antes!!
  if(posicoesIguais(pos, posTeletransporte[n][0]))
    return posTeletransporte[n][1];
  else
    return posTeletransporte[n][0];
}

static
void specialkey(int key, int x, int y)
{
  if(!alarmeSetado){
    setitimer(ITIMER_REAL, &tempo, NULL);
    alarmeSetado = true;
  }
  if(pausado) return;
  if(glutGetModifiers() & GLUT_ACTIVE_ALT){
    switch(key){
      case GLUT_KEY_LEFT:
        if(seMexendo) return;
        oldPosicao = posicao;
        posicao = caminha_mapa(vira_esquerda(direcao), posicao);
        if(oldPosicao.x != posicao.x || oldPosicao.z != posicao.z)
          seMexendo = passoSuave[vira_esquerda(direcao)] = true;
        break;
      case GLUT_KEY_RIGHT:
        if(seMexendo) return;
        oldPosicao = posicao;
        posicao = caminha_mapa(vira_direita(direcao), posicao);
        if(oldPosicao.x != posicao.x || oldPosicao.z != posicao.z)
          seMexendo = passoSuave[vira_direita(direcao)] = true;
        break;
    }
  }else{
    switch(key){
      case GLUT_KEY_UP:
        if(seMexendo) return;
        oldPosicao = posicao;
        posicao = caminha_mapa(direcao, posicao);
        if(oldPosicao.x != posicao.x || oldPosicao.z != posicao.z)
          seMexendo = passoSuave[direcao] = true;
        break;
      case GLUT_KEY_DOWN:
        if(seMexendo) return;
        oldPosicao = posicao;
        posicao = caminha_mapa(vira_direita(vira_direita(direcao)), posicao);
        if(oldPosicao.x != posicao.x || oldPosicao.z != posicao.z)
          seMexendo = passoSuave[vira_direita(vira_direita(direcao))] = true;
        break;
      case GLUT_KEY_LEFT:
        if(seMexendo) return;
        oldDirecao = direcao;
        seMexendo = rotSuaveEsq = true;
        direcao = vira_esquerda(direcao);
        break;
      case GLUT_KEY_RIGHT:
        if(seMexendo) return;
        oldDirecao = direcao;
        seMexendo = rotSuaveDir = true;
        direcao = vira_direita(direcao);
        break;
    }
  }
  if(isdigit(mapa[posicao.x][posicao.z]) && mapa[posicao.x][posicao.z] != '0'){
    if(!posicoesIguais(posicao, oldPosicao)
        && !posicoesIguais(oldPosicao, getParTele(posicao))){
      seMexendo = sendoTeletransportado = true;
      oldPosicao = posicao;
      posicao = getParTele(posicao);
      passoSuave[ESQUERDA] = passoSuave[DIREITA] =
        passoSuave[CIMA] = passoSuave[BAIXO] = false;
    }
  }else{
    switch(mapa[posicao.x][posicao.z]){
      case MAPA_BURACO:
        printf("AAAAAAAAAAhhhhhhhh!\n");
        seMexendo = estaCaindo = true;
        passoSuave[ESQUERDA] = passoSuave[DIREITA] =
          passoSuave[CIMA] = passoSuave[BAIXO] = false;
        break;
      case MAPA_FIM:
        setitimer(ITIMER_REAL, NULL, NULL);
        alarmeSetado = false;
        if(++faseAtual <= totalFases){
          if(!le_mapa(fases[faseAtual])){
            fprintf(stderr, "Erro ao carregar fase %s\n", fases[faseAtual]);
            exit(1);
          }
          criaLabirinto();
          posicao = posicaoInicial;
          direcao = direcaoInicial;
          seMexendo =
            passoSuave[ESQUERDA] = passoSuave[DIREITA] =
            passoSuave[CIMA] = passoSuave[BAIXO] = false;
        }else{
          printf("VOCE VENCEU, PARAB�NS\n"
"         .eec.      _\n"
"        cfffff+    | )/\\_\n"
"        f o o 8  _ | ) / >\n"
"        ( _U  / ( \\| ''.'/\n"
"         `---'   \\      |\n"
"     __  .-   `\\  `____/\n"
"   .'  )(       )(   /\n"
"  /   / )       ( `-'\n"
" /  .' /         \\\n"
" )-(  (         _/\n"
"/.||\\  \\____.--'\n"
" `''   (`-.__.--')\n"
"       |        (\n"
"       |    |   |\n"
"       (   /|   (\n"
"       <  ( <   \\\n"
"      /    )|    |\n"
"   __<`-._: |.--'-.\n"
"  (      |888L_    )\n"
"   `--'`'8888888aa'\n"
"\n\tCRIADORES:\n"
"Leonardo Alves Machado\n"
"Marcus Aurelius Cordenunsi Farias\n"
"Ascii arte de\n"
"http://www.ascii-art.de\n");
          exit(0);
        }
        break;
    }
  }
  glutPostRedisplay();
}

static
void keyboard(unsigned char key, int x, int y)
{
  switch(key){
    case 'p': case 'P':
      pausa(!pausado);
      break;
    case 27:
      exit(0);
  }
  glutPostRedisplay();
}

// Display list:
static const unsigned int LABIRINTO = 1;

static
void desenha(float angx, float angy, float x, float y, float z)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();
  glRotatef(angy, 0.f, 1.f, 0.f);
  if(angx != 0.f) // S� usado quando cai
    glRotatef(angx, 1.f, 0.f, 0.f);
  // Repare que x e z costumam ter sinais opostos...
  glTranslatef(-(x + .5f), y - .25f, z + .5f);
  glCallList(LABIRINTO);
  
  // Escrever o tempo (� _bem_ esquisito):
  if(tempo.it_value.tv_sec > 0){
    struct itimerval tempoRestante;
    const int strTempoMax = 32; // Suficiente pra mais de 2^64 com sinal
    char strTempo[strTempoMax];
    int strTempoLen = strTempoMax;
    if(pausado){
      // Escrever um 'p' caso esteja em pausa
      strTempo[0] = 'p'; strTempo[1] = '\0';
      strTempoLen = 1;
    }else{
      // Pega o tempo restante e converte pra string.
      getitimer(ITIMER_REAL, &tempoRestante);
      strTempoLen = snprintf(strTempo, strTempoMax, "%ld",
          tempoRestante.it_value.tv_sec + (tempoRestante.it_value.tv_usec > 1)); // Express�o booleana retorna 0 ou 1
    }
    if(strTempoLen > 0 && strTempoLen < strTempoMax){ // No caso imposs�vel de n�o caber, n�o fazer nada...
      int i;
      glDisable(GL_DEPTH_TEST);
      glMatrixMode(GL_PROJECTION);
      glPushMatrix();
      glLoadIdentity();
      gluOrtho2D(0., 100., 0., 100.); // Proje��o pra texto
      glMatrixMode(GL_MODELVIEW);
      glLoadIdentity();
      glColor3f(0.f, .5f, 0.f);
      glRasterPos2i(50, 10);
      for(i = 0; i < strTempoLen; ++i)
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, strTempo[i]);
      glMatrixMode(GL_PROJECTION);
      glPopMatrix(); // restaura a proje��o
      glMatrixMode(GL_MODELVIEW);
      glEnable(GL_DEPTH_TEST);
    }
  }
  glutSwapBuffers();
}

static
void display(void)
{
  const int MINI_PASSOS_TOT = 8;
  const float
    SOBE_DESCE = 1.f / 128.f,
    MINI_PASSO = 1.f / MINI_PASSOS_TOT,
    QUEDA = .25f;
  int angFim = direcao * 90;
  if(sendoTeletransportado){
    int i;
    // Gira 360� e...
    for(i = angFim + 10; i != angFim; i = (i + 10) % 360)
      desenha(0.f, i, posicao.x, 0.f, posicao.z);
    // ...busca dire��o que n�o tem parede:
    for(;;){
      if(posicoesIguais(caminha_mapa(direcao, posicao), posicao)){
        direcao = vira_direita(direcao);
        angFim = direcao * 90;
        while(i != angFim){
          i = (i + 10) % 360;
          desenha(0.f, i, posicao.x, 0.f, posicao.z);
        }
      }else
        break;
    }
    desenha(0.f, angFim, posicao.x, 0.f, posicao.z);
    seMexendo = sendoTeletransportado = false;
  }else if(estaCaindo){
    float i, y = 0.f;
    const float x = 9.f, z = 9.f, ang = ESQUERDA * 90;
    for(i = 0.f; i < 90.f; ++i){
      y += QUEDA;
      desenha(i, ang, x, y, z);
    }
    for(i = 0.f; i < 40.f; ++i){
      y += QUEDA;
      desenha(90.f, ang, x, y, z);
    }  
    seMexendo = estaCaindo = false;
    posicao = posicaoInicial;
    direcao = direcaoInicial;
    desenha(0.f, direcao * 90, posicao.x, 0.f, posicao.z);
  }else if(rotSuaveEsq || rotSuaveDir){
    int ang = oldDirecao * 90;
    while(ang != angFim){
      if(rotSuaveDir) ang = (ang + 10) % 360;
      else{
        if((ang -= 10) < 0) ang = 360 + ang;
      }
      desenha(0.f, ang, posicao.x, 0.f, posicao.z);
    }
    seMexendo = rotSuaveEsq = rotSuaveDir = false;
  }else if(passoSuave[CIMA] || passoSuave[BAIXO]){
    float pos = oldPosicao.x, y = 0.f;
    int i;
    for(i = 0; i < MINI_PASSOS_TOT; ++i){
      if(i < 4) y -= SOBE_DESCE;
      else      y += SOBE_DESCE;
      if(passoSuave[CIMA]) pos -= MINI_PASSO;
      else                 pos += MINI_PASSO;
      desenha(0.f, angFim, pos, y, posicao.z);
    }
    seMexendo = passoSuave[CIMA] = passoSuave[BAIXO] = false;
  }else if(passoSuave[ESQUERDA] || passoSuave[DIREITA]){
    float pos = oldPosicao.z, y = 0.f;
    int i;
    for(i = 0; i < MINI_PASSOS_TOT; ++i){
      if(i < 4) y -= SOBE_DESCE;
      else      y += SOBE_DESCE;
      if(passoSuave[ESQUERDA]) pos -= MINI_PASSO;
      else                     pos += MINI_PASSO;
      desenha(0.f, angFim, posicao.x, y, pos);
    }
    seMexendo = passoSuave[ESQUERDA] = passoSuave[DIREITA] = false;
  }else
    desenha(0.f, angFim, posicao.x, 0.f, posicao.z);
}

static
void criaLabirinto(void)
{
  int x, z;
  glNewList(LABIRINTO, GL_COMPILE);
  glBegin(GL_QUADS);
  for(x = 0; x < TM; ++x){
    for(z = 0; z < TM; ++z){
      if(isdigit(mapa[x][z]) && mapa[x][z] != '0'){
          glColor3f(.2f, .5f, .7f);
          
          glVertex3f(x, 0.f, -z);
          glVertex3f(x+1, 0.f, -z);
          glVertex3f(x+1, 0.f, -(z+1));
          glVertex3f(x, 0.f, -(z+1));
      }else{
        switch(mapa[x][z]){
          case MAPA_CAMINHO:
            glColor3f(.9f, .9f, 1.f);

            glVertex3f(x, 0.f, -z);
            glVertex3f(x+1, 0.f, -z);
            glVertex3f(x+1, 0.f, -(z+1));
            glVertex3f(x, 0.f, -(z+1));
            break;
          case MAPA_BURACO:
            glColor3f(0.f, 0.f, 0.f);

            glVertex3f(x, 0.f, -z);
            glVertex3f(x+1, 0.f, -z);
            glVertex3f(x+1, 0.f, -(z+1));
            glVertex3f(x, 0.f, -(z+1));
            break;
          case MAPA_INICIO:
            glColor3f(.7f, .9f, .8f);
            
            glVertex3f(x, 0.f, -z);
            glVertex3f(x+1, 0.f, -z);
            glVertex3f(x+1, 0.f, -(z+1));
            glVertex3f(x, 0.f, -(z+1));
            break;
          case MAPA_FIM:
            glColor3f(1.f, 0.f, 0.f);

            glVertex3f(x, 0.f, -z);
            glVertex3f(x+1, 0.f, -z);
            glVertex3f(x+1, 0.f, -(z+1));
            glVertex3f(x, 0.f, -(z+1));
            break;
          case MAPA_PAREDE:

            // Frente

            glColor3f(0.f, 0.f, .7f);

            glVertex3f(x, 0.f, -z);

            glColor3f(1.f, 1.f, 1.f);

            glVertex3f(x+1, 0.f, -z);        
            glVertex3f(x+1, .5f, -z);
            glVertex3f(x, .5f, -z);

            // Direita

            glColor3f(.4f, .7f, 1.f);

            glVertex3f(x+1, 0.f, -z);
            glVertex3f(x+1, 0.f, -(z+1));
            glVertex3f(x+1, .5f, -(z+1));

            glColor3f(1.f, 1.f, 1.f);

            glVertex3f(x+1, .5f, -z);

            // Tr�s

            glColor3f(.2f, .65f, 1.f);

            glVertex3f(x+1, .5f, -(z+1));

            glColor3f(1.f, 1.f, 1.f);

            glVertex3f(x+1, 0.f, -(z+1));
            glVertex3f(x, 0.f, -(z+1));
            glVertex3f(x, .5f, -(z+1));

            // Esquerda

            glColor3f(1.f, 1.f, 1.f);

            glVertex3f(x, .5f, -(z+1));

            glColor3f(.3f, .5f, .7f);

            glVertex3f(x, 0.f, -(z+1));
            glVertex3f(x, 0.f, -z);
            glVertex3f(x, .5f, -z);

            break;
        } // switch
      } // if-else
    } // for z
  } // for x
  glEnd();
  glEndList();
}

static
void reshape(int w, int h)
{
  if(h == 0) h = 1;
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.f, (float)w / (float)h, .1f, 100.f);
  glMatrixMode(GL_MODELVIEW);
}

static
void alarme(int x)
{
  printf("Seu tempo acabou.\n");
  exit(0);
}

static
void idle(void)
{
  if(!seMexendo) desenha(0.f, direcao * 90, posicao.x, 0.f, posicao.z);
}

static
void visibility(int state)
{
  if(state == GLUT_NOT_VISIBLE) pausa(true);
}

// vim: shiftwidth=2 expandtab
